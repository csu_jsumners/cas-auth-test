<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
<head>
  <title>CAS Secured Welcome Page</title>
</head>
<body>
  <p>This page is secured by CAS.</p>
  <p><a href="<spring:url value="/secured/logout" />">Logout</a></p>

  <c:if test="${uname != null}">
    <p>Username = <c:out value="${uname}" /></p>
  </c:if>

  <c:if test="${attributes != null}">
    <ul>
    <c:forEach var="key" items="${attributes.keySet()}">
      <li>
        <c:out value="key" /> =&gt; <c:out value="${attributes.get(key)}" />
      </li>
    </c:forEach>
    </ul>
  </c:if>
</body>
</html>