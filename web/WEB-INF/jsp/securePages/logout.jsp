<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
<head>
  <title>CAS Auth Test Logged Out</title>
</head>
<body>
  <p>You have been logged out of the CAS Auth Test but not CAS.</p>
  <p><a href="<spring:url value="/" />">Start Over</a></p>
</body>
</html>