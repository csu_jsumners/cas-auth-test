<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!doctype html>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <title>CAS Authentication Test</title>
</head>
<body>
  <p><a href="<spring:url value="/secured/welcome"/>">Secured Page</a></p>
</body>
</html>