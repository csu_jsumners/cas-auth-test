package org.howardism.nativetrust;

import javax.net.ssl.ManagerFactoryParameters;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactorySpi;
import java.security.*;

// http://howardism.org/Technical/Java/SelfSignedCerts.html
public class NativeTrustProvider extends Provider {
  private static final String TRUST_PROVIDER_ALG = "NativeTrustAlgorithm";
  private static final String TRUST_PROVIDER_ID = "NativeTrustProvider";
  /**
   * Constructs a provider with the specified name, version number,
   * and information.
   *
   * @param name    the provider name.
   * @param version the provider version number.
   * @param info    a description of the provider and its services.
   */
  protected NativeTrustProvider(String name, double version, String info) {
    super(name, version, info);
  }

  public NativeTrustProvider() {
    super(TRUST_PROVIDER_ID, 0.1, "NativeTrustProvider (screw your cert).");

    AccessController.doPrivileged(new PrivilegedAction<Object>() {
      @Override
      public Object run() {
        put(
            "TrustManagerFactory." + NativeTrustManagerFactory.getAlgorithm(),
            NativeTrustManagerFactory.class.getName());

        return null;
      }
    });
  }

  public static void setAlwaysTrust(boolean enableNativeTrustProvider) {
    if (enableNativeTrustProvider) {
      Provider registered = Security.getProvider(TRUST_PROVIDER_ID);
      if (null == registered) {
        Security.insertProviderAt(new NativeTrustProvider(), 1);
        Security.setProperty(
            "ssl.TrustManagerFactory.algorithm",
            TRUST_PROVIDER_ALG);
      }
    }
  }

  public final static class NativeTrustManagerFactory
      extends TrustManagerFactorySpi
  {

    @Override
    protected void engineInit(KeyStore keyStore) throws KeyStoreException {}

    @Override
    protected void engineInit(ManagerFactoryParameters managerFactoryParameters)
        throws InvalidAlgorithmParameterException {}

    @Override
    protected TrustManager[] engineGetTrustManagers() {
      return new TrustManager[] { new NativeTrustManager() };
    }

    public static String getAlgorithm() {
      return TRUST_PROVIDER_ALG;
    }
  }
}
