package edu.clayton.casauthtest.config;

import org.howardism.nativetrust.NativeTrustProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.*;
import java.util.EnumSet;

/**
 * This class defines the Java version of the traditional web.xml file.
 */
public class ContainerConfig implements WebApplicationInitializer {
  public static final Logger log = LoggerFactory.getLogger(ContainerConfig.class);

  public void onStartup(ServletContext servletContext) throws ServletException {
    NativeTrustProvider.setAlwaysTrust(true); // Eff your certificate.

    /// Load up our web application configuration files as our
    /// application context. Basically, add all of our explicitly defined
    /// Spring Beans that are required for application startup.
    AnnotationConfigWebApplicationContext context =
        new AnnotationConfigWebApplicationContext();
    context.register(ContextConfig.class, CasBeans.class);

    /// This is needed for the DelegatingFilterProxy objects.
    servletContext.addListener(new ContextLoaderListener(context));

    /// Filter for Single Sign Out requests from CAS.
    log.info("Setting up the CAS single sign out filter ...");
    DelegatingFilterProxy signOutDFP =
        new DelegatingFilterProxy("singleSignOutFilter");
    FilterRegistration signOutFR =
        servletContext.addFilter("singleSignOutFilter", signOutDFP);
    signOutFR.addMappingForUrlPatterns(
        EnumSet.of(DispatcherType.REQUEST),
        true,
        "/sso/*"
    );

    /// Filter for authentication. Any requests to "/secured/*" or "/sso/*"
    /// should only be served to clients that have authenticated, successfully,
    /// to the remote CAS server. This filter looks for non-authenticated
    /// requests and sends them to the CAS server for verification.
    log.info("Setting up CAS authentication filter ...");
    DelegatingFilterProxy authDFP =
        new DelegatingFilterProxy("authenticationFilter");
    FilterRegistration authFR =
        servletContext.addFilter("authenticationFilter", authDFP);
    authFR.addMappingForUrlPatterns(
        EnumSet.of(DispatcherType.REQUEST),
        true,
        "/secured/*"
    );
    authFR.addMappingForUrlPatterns(
        EnumSet.of(DispatcherType.REQUEST),
        true,
        "/sso/*"
    );

    /// Finish setting up the Spring WebMVC environment with a
    /// request dispatcher.
    DispatcherServlet dispatcherServlet = new DispatcherServlet(context);

    ServletRegistration.Dynamic dispatcher =
        servletContext.addServlet("dispatcher", dispatcherServlet);

    dispatcher.setLoadOnStartup(1);
    dispatcher.addMapping("/");
  }
}
