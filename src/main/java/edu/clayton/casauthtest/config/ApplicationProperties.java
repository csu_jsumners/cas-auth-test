package edu.clayton.casauthtest.config;

import com.jrfom.util.Resloader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

/***
 * A simple class to abstract our configurable application.properties file.
 */
@Component
public class ApplicationProperties {
  private static final Logger log = LoggerFactory.getLogger(ApplicationProperties.class);

  private String casServerUrl;
  private String loginUrl;
  private String logoutUrl;
  private String serviceUrl;

  public ApplicationProperties() {
    String propertiesFile;
    try {
      propertiesFile =
          Resloader.loadString(ContextConfig.class, "application.properties");
    } catch (FileNotFoundException e) {
      log.error("Couldn't load application.properties!");
      log.error(e.toString());

      return;
    }

    Properties properties = new Properties();
    try {
      properties.load(new StringReader(propertiesFile));
    } catch (IOException e) {
      log.error("Couldn't populate Properties object!");
      log.error(e.toString());

      return;
    }

    this.casServerUrl = properties.getProperty("cas.serverUrl");
    this.loginUrl = properties.getProperty("cas.loginUrl");
    this.logoutUrl = properties.getProperty("cas.logoutUrl");
    this.serviceUrl = properties.getProperty("cas.serviceUrl");
  }

  public String getCasServerUrl() {
    return this.casServerUrl;
  }

  public String getLoginUrl() {
    return this.loginUrl;
  }

  public String getLogoutUrl() {
    return this.logoutUrl;
  }

  public String getServiceUrl() {
    return this.serviceUrl;
  }
}
