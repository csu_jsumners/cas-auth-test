package edu.clayton.casauthtest.config;

import org.jasig.cas.client.authentication.AuthenticationFilter;
import org.jasig.cas.client.session.SingleSignOutFilter;
import org.jasig.cas.client.util.HttpServletRequestWrapperFilter;
import org.jasig.cas.client.validation.Cas20ServiceTicketValidator;
import org.jasig.cas.client.validation.Saml11TicketValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This class defines all of the CAS specific Beans that will be used
 * by the web application.
 */
@Configuration
public class CasBeans {
  @Autowired
  private ApplicationProperties applicationProperties;

  /// This is the primary filter for dealing with the remote CAS server.
  /// Every request that comes in will be processed by this filter to
  /// determine if it is an authorized request. For more details on how
  /// this works, see the discussion in SingleSignOnController.java.
  @Bean
  public AuthenticationFilter authenticationFilter() {
    AuthenticationFilter authenticationFilter = new AuthenticationFilter();

    authenticationFilter.setCasServerLoginUrl(applicationProperties.getLoginUrl());
    authenticationFilter.setRenew(false);
    authenticationFilter.setGateway(false);
    authenticationFilter.setService(applicationProperties.getServiceUrl());

    return authenticationFilter;
  }

  /// We will use this Bean to validate Service Tickets with the remote
  /// CAS server. It does all of the dirty work in sending the ticket back
  /// to the server and parsing the resulting message. Again, further details
  /// are available in SingleSignOnController.java.
  @Bean
  public Cas20ServiceTicketValidator serviceTicketValidator() {
    Cas20ServiceTicketValidator validator =
        new Cas20ServiceTicketValidator(applicationProperties.getCasServerUrl());

    return validator;
  }

  /// This Bean looks for sign out POST requests coming from the remote CAS
  /// server. When such a request is received, this filter will remove the
  /// CAS Assertion object from the user's session. Thus, the user will be
  /// logged out of this application.
  @Bean
  public SingleSignOutFilter singleSignOutFilter() {
    return new SingleSignOutFilter();
  }
}
