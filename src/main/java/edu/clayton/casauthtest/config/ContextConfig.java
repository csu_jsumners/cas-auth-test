package edu.clayton.casauthtest.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * This class defines our most basic Spring Beans and tells Spring which
 * package(s) to scan for Spring Web MVC components (e.g. Controllers).
 */
@Configuration
@EnableWebMvc
@ComponentScan (basePackages = {
    "edu.clayton.casauthtest"
})
public class ContextConfig {
  private static final Logger log = LoggerFactory.getLogger(ContextConfig.class);

  @Bean
  public ViewResolver viewResolver() {
    InternalResourceViewResolver vr = new InternalResourceViewResolver();
    vr.setPrefix("/WEB-INF/jsp/");
    vr.setSuffix(".jsp");

    return vr;
  }

  @Bean
  public ApplicationProperties applicationProperties() {
    return new ApplicationProperties();
  }
}
