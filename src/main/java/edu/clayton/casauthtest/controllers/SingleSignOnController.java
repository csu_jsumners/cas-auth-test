package edu.clayton.casauthtest.controllers;

import edu.clayton.casauthtest.config.ApplicationProperties;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.validation.Assertion;
import org.jasig.cas.client.validation.Cas20ServiceTicketValidator;
import org.jasig.cas.client.validation.TicketValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Map;

/// This class will be used as our CAS service end point. We will tell
/// the remote CAS server to use the URL provided by this controller as
/// the service for our application. Thus, CAS will send the Service Ticket
/// to this controller when a user has successfully authenticated.
@Controller
public class SingleSignOnController {
  private static final Logger log = LoggerFactory.getLogger(SingleSignOnController.class);

  @Autowired
  private ApplicationProperties applicationProperties;
  @Autowired
  private Cas20ServiceTicketValidator serviceTicketValidator;

  @RequestMapping(value = "/sso/login")
  public ModelAndView handleLogin(WebRequest webRequest, HttpSession session) {
    ModelAndView mav = new ModelAndView();

    if (!webRequest.getParameterMap().containsKey("ticket")) {
      // What? How did you get here?
      mav.setViewName("ssoError");

      /// This case should never be executed because the CAS client does
      /// authentication through a Java Servlet "Filter"[1]. Thus, any request
      /// to "/sso/login" without a "ticket" parameter should be forwarded to
      /// the remote CAS server before this controller is ever executed.
      ///
      /// We're doing this check, and showing the "ssoError" view, just in case
      /// the user somehow were to get around the filter. It also provides a
      /// convenient way to determine that we do have a Service Ticket.
      ///
      /// [1] - http://docs.oracle.com/javaee/5/api/javax/servlet/Filter.html
    } else {
      // We need to validate that ticket.
      Principal principal = null;
      Map<String, Object> attributes = null;

      try {
        /// CAS validation responses are encapsulated in an Assertion object.
        /// When the authentication filter processes incoming requests, it
        /// looks for an instance of a CAS Assertion in the session associated
        /// with that request. If one is found, the authentication filter
        /// passes the request on down the filter chain. Thus, the presence of
        /// an Assertion in the user's session bypasses any further checks
        /// with the remote CAS server (unless a later filter does so).
        ///
        /// Note that we will not get an Assertion object unless the remote
        /// CAS server says the user is authenticated.
        Assertion assertion = serviceTicketValidator.validate(
            webRequest.getParameter("ticket"),
            applicationProperties.getServiceUrl()
        );

        session.setAttribute(AbstractCasFilter.CONST_CAS_ASSERTION, assertion);

        principal = assertion.getPrincipal();
        attributes = assertion.getAttributes();

        String username = (principal != null) ? principal.getName() : null;

        mav.addObject("username", username);
        mav.addObject("attributes", attributes);

        // Now that we have validated user we can send them to the
        // secured area of the application.
        mav.setView(new RedirectView("/secured/welcome", false, false, false));
      } catch (TicketValidationException e) {
        log.error("Could not validate service ticket!");
        log.error(e.toString());

        mav.setViewName("ssoError");
      }
    }

    return mav;
  }
}
