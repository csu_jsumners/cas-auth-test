package edu.clayton.casauthtest.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * This class provides the controller for the whole of our simple
 * secured site. Any view served by this controller will only be served
 * to authorized requests by virtue of the CAS authentication filter.
 */
@Controller
public class SecuredSite {
  @RequestMapping(value = "/secured")
  public String securedIndex() {
    return "forward:/secured/welcome";
  }

  @RequestMapping(value = "/secured/logout")
  public String logoutPage(HttpSession session) {
    session.invalidate();

    return "securePages/logout";
  }

  @RequestMapping(value = "/secured/welcome")
  public String welcomePage() {
    return "securePages/welcome";
  }
}
