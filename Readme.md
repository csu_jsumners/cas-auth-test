# CAS Auth Test #

This is a simple Spring Web MVC 3.1 application that demonstrates how to use the [CAS Client](https://wiki.jasig.org/display/CASC/CAS+Client+for+Java+3.1) for authentication.

This application will authenticate via a specified CAS server and present the user with a single "secure" page. This "secure" page provides a logout link that will log a user out of the application but not out of the CAS server. Additionally, you can use the CAS server's single sign out feature to log out of this test application.

## Project Notes ##

### Config ###

The web application depends on a valid "application.properties" file for proper operation (it won't even start without one). There is an example file, "application.properties.example", in `src/main/resources/edu/clayton/casauthtest/config/`. Simple copy this file to "application.properties" in the same directory and change the values in the new file to match your environment.

### JBSResloader ###

This project depends on [JBSResloader](https://bitbucket.org/jsumners/jbsresloader). This library is not currently available in Maven Central. Thus, you will have to [download it](https://bitbucket.org/jsumners/jbsresloader/downloads) separately and add it to your local repository.

Alternatively, you can modify the `ApplicationProperties.java` file to remove the dependency. It isn't a difficult modification.

## License ##

The MIT License (MIT)

Copyright (c) 2013 Clayton State University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.